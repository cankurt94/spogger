﻿using Spogger.Model;
using Spogger.Web.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Spogger.Web.Controllers
{
    public class CategoryController : Controller
    {
        private List<Category> categories;
        public CategoryController()
        {
            categories = new List<Category>();

            categories.Add(new Category
            {
                Description = "blabla",
                DisplayName = "Dövüş Sporları",
                Id = 1,
                Priority = 1,
                ParentId = 0
            });
            categories.Add(new Category
            {
                Description = "blabla",
                DisplayName = "Boks",
                Id = 2,
                Priority = 1,
                ParentId = 1
            });
            categories.Add(new Category
            {
                Description = "blabla",
                DisplayName = "Fitness",
                Id = 3,
                Priority = 1,
                ParentId = 1
            });
            categories.Add(new Category
            {
                Description = "blabla",
                DisplayName = "Atletizm",
                Id = 4,
                Priority = 2,
                ParentId = 0
            });
            categories.Add(new Category
            {
                Description = "blabla",
                DisplayName = "Engelli Koşu",
                Id = 5,
                Priority = 1,
                ParentId = 4
            });
        }
        public ActionResult Index()
        {
            var viewModel = new CategoryViewModel();

            viewModel.Categories = new List<Category>();

            viewModel.Categories.Add(
                new Category
                {
                    Description = "Boks Kategorisi",
                    DisplayName = "Boks",
                    Id = 1,
                    ImagePath = "http://www.fit4ever.com.tr/foto/kick-boks-0258.jpg",
                    Priority = 1
                });

            viewModel.Categories.Add(
                new Category
                {
                    Description = "Kick-Boks Kategorisi",
                    DisplayName = "Kick-Boks",
                    Id = 2,
                    ImagePath = "http://www.nkfu.com/wp-content/uploads/2015/03/kickboks.jpg",
                    Priority = 2
                });

            return View(viewModel);
        }

        public ActionResult Detail(int id)
        {
            var category = this.categories.FirstOrDefault(x => x.Id == id);

            return View(category);
        }

        public ActionResult GetHeaderMenu()
        {
            return PartialView(this.categories);
        }
    }
}