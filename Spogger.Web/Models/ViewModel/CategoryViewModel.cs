﻿using Spogger.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Spogger.Web.Models.ViewModel
{
    public class CategoryViewModel
    {
        public List<Category> Categories { get; set; }
    }
}