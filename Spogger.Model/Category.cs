﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spogger.Model
{
    public class Category
    {
        public int Id { get; set; }
        public string DisplayName { get; set; }
        public int Priority { get; set; }
        public string ImagePath { get; set; }
        public string Description { get; set; }
        public int ParentId { get; set; }
    }
}